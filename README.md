# Windows commands

What it does:
- Runs commands on local OS of windows server configured as a gitlab-runner with powershell as the executor type. Not very exciting, but good for basic test to make sure connectivity and access to windows runner is healthy.

Setup:
- On windows host install git and set git in environment variables path. 
- In settings -> cicd -> runners, get token for new runner setup.
- Download the gitlab-runner.exe.
- Put .exe into C:\gitlab.
- Run gitlab-runner.exe install, then gitlab-runner.exe register.
  - use the default gitlab url, paste in token, add tag to id the runner, "shell" for executor type.
- For windows 2016 modify the config.toml file. Replace "pwsh" with "powershell"

Sequence:
- Echo commands.
- Check temp dir contents.
- Run simple powershell script to create a directory in temp dir.
- Check temp dir to verify file was created. 
